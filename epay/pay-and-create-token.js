module.exports = {
  type: 'object',
  properties: {
    merId: {
      type: 'string',
      maxLength: 10,
    },
    currency: {
      type: 'string',
      maxLength: 3,
    },
    amount: {
      type: 'string',
      maxLength: 12,
    },
    invoiceNo: {
      type: 'string',
      maxLength: 40,
    },
    goodsNm: {
      type: 'string',
      maxLength: 200,
    },

    payType: {
      type: 'string',
      maxLength: 2,
    },
    buyerEmail: {
      type: 'string',
      maxLength: 40,
    },
    callBackUrl: {
      type: 'string',
      maxLength: 255,
    },
    notiUrl: {
      type: 'string',
      maxLength: 255,
    },
    reqDomain: {
      type: 'string',
      maxLength: 255,
    },
    vat: {
      type: 'number',
      maximum: 999999999999,
    },
    fee: {
      type: 'number',
      maximum: 999999999999,
    },
    description: {
      type: 'string',
      maxLength: 100,
    },
    merchantToken: {
      type: 'string',
      maxLength: 255,
    },
    userIP: {
      type: 'string',
      maxLength: 15,
    },
    userLanguage: {
      type: 'string',
      maxLength: 2,
    },
    timeStamp: {
      type: 'string',
      maxLength: 13,
    },
    merTrxId: {
      type: 'string',
      maxLength: 50,
    },
    userFee: {
      type: 'string',
      maxLength: 12,
    },
    goodsAmount: {
      type: 'string',
      maxLength: 12,
    },
    windowColor: {
      type: 'string',
      maxLength: 7,
    },
    windowType: {
      type: 'string',
      maxLength: 2,
    },
    payToken: {
      type: 'string',
      maxLength: 100,
    },
  },
};
