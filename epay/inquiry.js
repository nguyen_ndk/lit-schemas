module.exports = {
  type: 'object',
  required: ['merId', 'merTrxId', 'timeStamp', 'merchantToken'],
  properties: {
    merId: {
      type: 'string',
      maxLength: 10,
    },
    merTrxId: {
      type: 'string',
      maxLength: 50,
    },
    timeStamp: {
      type: 'string',
      maxLength: 13,
    },
    merchantToken: {
      type: 'string',
      maxLength: 255,
    },
  },
};
