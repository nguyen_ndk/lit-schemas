module.exports = {
  epay: {
    payAndCreateToken: require('./epay/pay-and-create-token'),
    charge: require('./epay/charge'),
    refund: require('./epay/refund'),
    inquiry: require('./epay/inquiry'),
  },
};
