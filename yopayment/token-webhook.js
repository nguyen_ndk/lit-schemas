module.exports = {
  type: 'object',
  properties: {
    TransactionID: { type: 'string' },
    data: { type: 'string' },
    checksum: { type: 'string' },
    errorCode: { type: 'string' },
    message: { type: 'string' },
  },
  if: {
    allOf: [{ required: ['message'] }],
  },
  then: {
    required: ['errorCode', 'message', 'TransactionID'],
  },
  else: {
    required: ['data', 'checksum', 'TransactionID'],
  },
};
